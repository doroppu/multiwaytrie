/*
 * MultiwayTrie.h
 */

#ifndef MULTIWAYTRIE_H_
#define MULTIWAYTRIE_H_

#include <string>
using namespace std;

class Node {
    public:
        bool word; 
        Node* children[26];
        Node(); 
        ~Node();
};

Node::Node(void) {
    word = false;
    for(int i = 0; i < 26; ++i) {
        children[i] = NULL;
    }
}

Node::~Node(void) {
    for(int i = 0; i < 26; ++i) {
        delete children[i];
    }
    delete this;
}

class MultiwayTrie {
    public:
        Node* root; 
        bool find(string word);  
        void insert(string word); 
        void remove(string word); 
        MultiwayTrie();    
        ~MultiwayTrie();
};

MultiwayTrie::MultiwayTrie(void) {
    root = new Node();
}

MultiwayTrie::~MultiwayTrie(void) {
    delete root;
}

bool MultiwayTrie::find(string word) {
	Node *curr = root;

    for (int i = 0; i < word.length(); i++) {
        int index = word[i] - 'a';
        if (!curr->children[index])
            return false;

        curr = curr->children[index];
    }

    return (curr != NULL && curr->word);
}

void MultiwayTrie::insert(string word) {
    Node *curr = root;

    for (int i = 0; i < word.length(); i++)
    {
        int index = word[i] - 'a';
        if (!curr->children[index]){
        	curr->children[index] = new Node;
        }
        curr = curr->children[index];
    }
    if (!curr->word)
    	curr->word = true;
}

void MultiwayTrie::remove(string word) {
	Node *curr = root;

    for (int i = 0; i < word.length(); i++)
    {
        int index = word[i] - 'a';
        if (!curr->children[index]){
        	return;
        } else {
        	curr = curr->children[index];
        }
    }
    if (curr->word)
    	curr->word = false;
}

#endif /* MULTIWAYTRIE_H_ */
